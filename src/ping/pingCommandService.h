#pragma once

#include "Arduino.h"

#include "commands/commandService.h"

#include "pingMessage.h"

class PingCommandService: public CommandService {
public:
    PingCommandService();
};

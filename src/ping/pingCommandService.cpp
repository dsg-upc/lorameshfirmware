#include "pingCommandService.h"
#include "ping.h"

PingCommandService::PingCommandService() {
    addCommand(Command("/pingOn", "Set the Ping On specifying the source in hex (like the display)", PingCommand::POn, 1,
        [this](String args) {
        return String(Ping::getInstance().pingOn(strtol(args.c_str(), NULL, 16)));
    }));

    addCommand(Command("/pingOff", "Set the Ping Off specifying the source in hex (like the display)", PingCommand::POff, 1,
        [this](String args) {
        return String(Ping::getInstance().pingOff(strtol(args.c_str(), NULL, 16)));
    }));
}

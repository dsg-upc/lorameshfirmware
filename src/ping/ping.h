#pragma once

#include <Arduino.h>

#include <ArduinoLog.h>

#include "sensor.h"

#include "message/messageService.h"

#include "message/messageManager.h"

#include "pingCommandService.h"

#include "pingMessage.h"

#include "config.h"

#include "LoraMesher.h"

class Ping : public MessageService {
public:
    /**
     * @brief Construct a new GPSService object
     *
     */
    static Ping& getInstance() {
        static Ping instance;
        return instance;
    }

    PingCommandService* pingCommandService = new PingCommandService();

    void init();

    String pingOn();

    String pingOn(uint16_t dst);

    String pingOff();

    String pingOff(uint16_t dst);

    void createAndSendPing();

    String getJSON(DataMessage* message);

    DataMessage* getDataMessage(JsonObject data);

    DataMessage* getPingMessage(PingCommand command, uint16_t dst);

    void processReceivedMessage(messagePort port, DataMessage* message);

private:
    Ping() : MessageService(PingApp, "Ping") {
        commandService = pingCommandService;
    };

    uint8_t pingId = 0;

    PingCommand pingCommandS;
    u_int32_t echovalueS;

};

    
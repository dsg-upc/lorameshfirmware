#include "ping.h"

void Ping::init() {
    pinMode(LED, OUTPUT);
}

String Ping::pingOn() {
    digitalWrite(LED, LED_ON);
    Log.verboseln(F("Ping On"));
    return "Ping On";
}

String Ping::pingOn(uint16_t dst) {
    if (dst == LoraMesher::getInstance().getLocalAddress())
        return pingOn();

    DataMessage* msg = getPingMessage(PingCommand::POn, dst);
    MessageManager::getInstance().sendMessage(messagePort::LoRaMeshPort, msg);

    delete msg;

    return "Ping On";
}

String Ping::pingOff() {
    digitalWrite(LED, LED_OFF);
    Log.verboseln(F("Ping Off"));
    return "Ping Off";
}

String Ping::pingOff(uint16_t dst) {
    if (dst == LoraMesher::getInstance().getLocalAddress())
        return pingOff();

    DataMessage* msg = getPingMessage(PingCommand::POff, dst);
    MessageManager::getInstance().sendMessage(messagePort::LoRaMeshPort, msg);

    delete msg;

    return "Ping Off";
}

String Ping::getJSON(DataMessage* message) {
    Log.verboseln(F("FF: Ping::getJSON"));
    PingMessage* pingMessage = (PingMessage*) message;

    StaticJsonDocument<400> doc;  // FF: was 200
    //DynamicJsonDocument doc(400); // FF: do not make dynamic

    JsonObject data = doc.createNestedObject("data");

    pingMessage->serialize(data);

    String json;
    serializeJson(doc, json);

    return json;
}

DataMessage* Ping::getDataMessage(JsonObject data) {
    PingMessage* pingMessage = new PingMessage();
    Log.infoln(F("Ping::getDataMessage new PingMessage()") );
    //Log.infoln(F("FF: Heap after new PingMessage() getFreeHeap() :%d"), ESP.getFreeHeap() );
    Log.infoln(F("FF: Heap after new PingMessage(): %d %d %d %d"),ESP.getFreeHeap(), ESP.getMinFreeHeap(), ESP.getHeapSize(), ESP.getMaxAllocHeap());

    pingMessage->deserialize(data);  //FF commented.

    pingMessage->messageSize = sizeof(PingMessage) - sizeof(DataMessageGeneric);

    return ((DataMessage*) pingMessage);
}

DataMessage* Ping::getPingMessage(PingCommand command, uint16_t dst) {
    PingMessage* pingMessage = new PingMessage();
    Log.infoln(F("FF Ping::getPingMessage: new PingMessage()") );

    pingMessage->messageSize = sizeof(PingMessage) - sizeof(DataMessageGeneric);

    pingMessage->pingCommand = command;

    pingMessage->appPortSrc = appPort::PingApp;
    pingMessage->appPortDst = appPort::PingApp;

    pingMessage->addrSrc = LoraMesher::getInstance().getLocalAddress();
    Log.infoln(F("Ping::getPingMessage: pingMessage->addrSrc: %d"), pingMessage->addrSrc );
    pingMessage->addrDst = dst;

    return (DataMessage*) pingMessage;
}

void Ping::processReceivedMessage(messagePort port, DataMessage* message) {
    PingMessage* pingMessage = (PingMessage*) message;

    Log.infoln(F("FF: Ping::processReceivedMessage  perform local actions") );

    pingCommandS = pingMessage->pingCommand;   //FF added
    echovalueS = pingMessage->echovalue;    //FF added
    
    Log.verboseln(F("FF in Ping::processReceivedMessage pingCommandS %d"),pingCommandS );
    Log.verboseln(F("FF in Ping::processReceivedMessage echovalueS %d"),echovalueS);

    switch (pingMessage->pingCommand) {
        case PingCommand::POn:
            pingOn();
            break;
        case PingCommand::POff:
            pingOff();
            break;
        default:
            break;
    }
    
    //delete message;  // FF tried, makes segmentation fault.
    //delete pingMessage; // FF tried, 

    //Ping& ping = Ping::getInstance();
    //ping.
    Log.infoln(F("FF: Ping::processReceivedMessage   createAndSendPing()") );
    createAndSendPing();
}

void Ping::createAndSendPing() {
    // uint8_t metadataSize = 1; //TODO: This should be dynamic with an array of sensors
    // uint16_t metadataSensorSize = metadataSize * sizeof(MetadataSensorMessage);
    uint16_t messageWithHeaderSize = sizeof(PingMessage);// + metadataSensorSize;

    //PingMessage* message = (PingMessage*) malloc(messageWithHeaderSize);
    PingMessage* message = new PingMessage();  // FF: instead of malloc

    Log.verboseln(F("FF Ping::createAndSendPing: Sending ping message"));

    if (message) {

        message->appPortDst = appPort::MQTTApp;
        message->appPortSrc = appPort::PingApp;
        message->addrSrc = LoraMesher::getInstance().getLocalAddress();
        //Log.verboseln(F("Ping::createAndSendPing message->addrSrc %d"),message->addrSrc);
        message->addrDst = 0;
        message->messageId = pingId++;

        message->pingCommand = pingCommandS;   //FF added
        message->echovalue = echovalueS;    //FF added


        message->messageSize = messageWithHeaderSize - sizeof(DataMessageGeneric);
        //message->gps = GPSService::getInstance().getGPSMessage();
        //message->flSendTimeInterval = FL_UPDATE_DELAY;

        // message->metadataSize = metadataSize;

        //TODO: This should be a vector of sensors
        // Temperature& temperature = Temperature::getInstance();

        // MetadataSensorMessage* tempMetadata = temperature.getMetadataMessage();
        // memcpy(message->sensorMetadata, tempMetadata, sizeof(MetadataSensorMessage));

        // signData(message);

        MessageManager::getInstance().sendMessage(messagePort::MqttPort, (DataMessage*) message);

        //free(message);
        delete message;
    }
    else {
        Log.errorln(F("Failed to allocate memory for ping message"));
    }
}
#pragma once

// Choose the device
#define T_BEAM_V10 // ttgo-t-beam
//#define T_BEAM_LORA_32 // ttgo-lora32-v1
// #define NAYAD_V1

// Configuration

#ifdef NAYAD_V1
#define I2C_SDA 02
#define I2C_SCL 04
#elif defined(T_BEAM_V10) || defined(T_BEAM_LORA_32)
#define I2C_SDA SDA
#define I2C_SCL SCL
#endif

//If the device has a GPS module
// #define GPS_ENABLED
#if defined(T_BEAM_V10)
#define GPS_TX 12
#define GPS_RX 34
#elif defined(NAYAD_V1)
#define GPS_RX 25
#define GPS_TX 35
#endif
#define GPS_BAUD 9600

#if defined(T_BEAM_LORA_32) && defined(GPS_ENABLED)
#warning "GPS in T_BEAM_LORA_32 is not default supported"
#endif

// #define BLUETOOTH_ENABLED

#define DISPLAY_ENABLED
#if defined(T_BEAM_LORA_32)
#define DISPLAY_SDA SDA
#define DISPLAY_SCL SCL
#define DISPLAY_RST OLED_RST
#elif defined(NAYAD_V1) || defined(T_BEAM_V10)
#define DISPLAY_SDA I2C_SDA
#define DISPLAY_SCL I2C_SCL
#define DISPLAY_RST -1
#endif


//WiFi Configuration
#define MAX_CONNECTION_TRY 5

// WiFi credentials
//#define WIFI_SSID "zUPC-CN-C6-E206-Alix"
#define WIFI_SSID ""
#define WIFI_PASSWORD ""


// MQTT configuration
#define MQTT_ENABLED
//#define MQTT_SERVER "10.1.24.145"
#define MQTT_SERVER "192.168.68.122"
#define MQTT_PORT 1883
#define MQTT_USERNAME "admin"
#define MQTT_PASSWORD "public"
#define MQTT_TOPIC_SUB "from-server/"
#define MQTT_TOPIC_OUT "to-server/"
#define MQTT_MAX_PACKET_SIZE 512 // 128, 256 or 512
#define MQTT_MAX_QUEUE_SIZE 10
#define MQTT_STILL_CONNECTED_INTERVAL 0 // In milliseconds, 0 to disable

//Wallet Configuration
#ifndef A0
#define A0 A0
#endif

//Sensors Configuration
#define STORED_SENSOR_DATA 10
    //- Temperature Configuration
//#define TEMPERATURE_ENABLED  //FF for sensorold
//#define SENSORS_ENABLED //FF for new sensor
#define SOIL_SENSOR_PIN 12 //FF was 0
#define TEMP_UPDATE_DELAY 120000 //ms  //FF for sensorold
#define SENSOR_SENDING_EVERY 60000 //ms
    //- Metadata Configuration
//define METADATA_ENABLED  // FF activated
#define METADATA_UPDATE_DELAY  6000// 600000 //ms  FF in LC: 300000 //ms

//LoRaChat Configuration
// #define LORACHAT_ENABLED
#define MAX_NAME_LENGTH 10
#define MAX_MESSAGE_LENGTH 100
#define MAX_PREVIOUS_MESSAGES 10


//Cdp configuration
#define CDP_ENABLED
#define CDP_UPDATE_DELAY 20000 //10000 //ms   when periodic task

//Ping configuration
#define PING_ENABLED

//Logging Configuration
// #define DISABLE_LOGGING

// Led configuration
#define LED_ENABLED
#if defined(NAYAD_V1)
#define LED 4
#define LED_ON      LOW
#define LED_OFF     HIGH
#elif defined(T_BEAM_LORA_32)
#define LED LED_BUILTIN
#define LED_ON      HIGH
#define LED_OFF     LOW
#elif defined(T_BEAM_V10)
#define LED 4
#define LED_ON      LOW
#define LED_OFF     HIGH
#endif

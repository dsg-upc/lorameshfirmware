#include <Arduino.h>

//Configuration
#include "config.h"

//Log
#include "ArduinoLog.h"

//Helpers
#include "helpers/helper.h"

//Manager
#include "message/messageManager.h"

//LoRaMesh
#include "loramesh/loraMeshService.h"

//WiFi
#include "wifi/wifiServerService.h"

//Wallet
#include "wallet/wallet.h"

//Temperature
#include "sensorlto/temperature.h"

//Metadata
#include "sensorlto/metadata.h"

#ifdef LED_ENABLED
#pragma region Led
#include "led/led.h"

Led& led = Led::getInstance();

void initLed() {
    led.init();
}

#pragma endregion
#endif

#ifdef CDP_ENABLED
#pragma region Cpd
#include "cdp/cdp.h"

Cdp& cdp = Cdp::getInstance();

void initCdp() {
    cdp.initCdp();
}

#pragma endregion
#endif

#ifdef PING_ENABLED
#pragma region Ping
#include "ping/ping.h"

Ping& ping = Ping::getInstance();

void initPing() {
    ping.init();
}

#pragma endregion
#endif

#pragma region Wallet

Wallet& wallet = Wallet::getInstance();

void initWallet() {
    wallet.begin();
}

#pragma endregion

#pragma region Metadata

Metadata& metadata = Metadata::getInstance();

void initMetadata() {
    metadata.initMetadata();
}

#pragma endregion

#pragma region Temperature
Temperature& temperature = Temperature::getInstance();

void initTemperature() {
    temperature.initTemperature();
}

#pragma endregion

#pragma region WiFi

WiFiServerService& wiFiService = WiFiServerService::getInstance();

void initWiFi() {
    wiFiService.initWiFi();
}

#pragma endregion

#pragma region LoRaMesher

LoRaMeshService& loraMeshService = LoRaMeshService::getInstance();

void initLoRaMesher() {
    //Init LoRaMesher
    loraMeshService.initLoraMesherService();
}

#pragma endregion

#ifdef MQTT_ENABLED
#include "mqtt/mqttService.h"

MqttService& mqttService = MqttService::getInstance();

void initMQTT() {
    mqttService.initMqtt(String(loraMeshService.getLocalAddress()));
}
#endif


#pragma region GPS

#include "gps/gpsService.h"

#define UPDATE_GPS_DELAY 10000 //ms

GPSService& gpsService = GPSService::getInstance();
#ifdef GPS_ENABLED

void initGPS() {
    //Initialize GPS
    gpsService.initGPS();
}
#endif

#pragma endregion

#ifdef BLUETOOTH_ENABLED
#pragma region SerialBT
#include "bluetooth/bluetoothService.h"
BluetoothService& bluetoothService = BluetoothService::getInstance();

void initBluetooth() {
    bluetoothService.initBluetooth(String(loraMeshService.getLocalAddress(), HEX));
}

#pragma endregion
#endif

#pragma region Manager

MessageManager& manager = MessageManager::getInstance();

void initManager() {
    manager.init();
    Log.verboseln("Manager initialized");

#ifdef BLUETOOTH_ENABLED
    manager.addMessageService(&bluetoothService);
    Log.verboseln("Bluetooth service added to manager");
#endif

    manager.addMessageService(&gpsService);
    Log.verboseln("GPS service added to manager");

    manager.addMessageService(&loraMeshService);
    Log.verboseln("LoRaMesher service added to manager");

    manager.addMessageService(&wiFiService);
    Log.verboseln("WiFi service added to manager");

#ifdef MQTT_ENABLED
    manager.addMessageService(&mqttService);
    Log.verboseln("MQTT service added to manager");
#endif

    manager.addMessageService(&wallet);
    Log.verboseln("Wallet service added to manager");

#ifdef LED_ENABLED
    manager.addMessageService(&led);
    Log.verboseln("Led service added to manager");
#endif

#ifdef PING_ENABLED
    manager.addMessageService(&ping);
    Log.verboseln("Ping service added to manager");
#endif

#ifdef CDP_ENABLED
    manager.addMessageService(&cdp);
    Log.verboseln("Cdp service added to manager");
#endif

    manager.addMessageService(&metadata);
    Log.verboseln("Metadata service added to manager");

    manager.addMessageService(&temperature);
    Log.verboseln("Temperature service added to manager");

    Serial.println(manager.getAvailableCommands());
}

#pragma endregion

// #pragma region AXP20x
// #include <axp20x.h>

// AXP20X_Class axp;

// void initAXP() {
//     Wire.begin(21, 22);
//     if (axp.begin(Wire, AXP192_SLAVE_ADDRESS) == AXP_FAIL) {
//         Serial.println(F("failed to initialize communication with AXP192"));
//     }
//     Serial.println(axp.getBattVoltage());
// }

// float getBatteryVoltage() {
//     return axp.getBattVoltage();
// }

// #pragma endregion

#ifdef DISPLAY_ENABLED
#pragma region Display
//Display
#include "display.h"

TaskHandle_t display_TaskHandle = NULL;

#define DISPLAY_TASK_DELAY 50 //ms
#define DISPLAY_LINE_TWO_DELAY 10000 //ms
#define DISPLAY_LINE_THREE_DELAY 50000 //ms
#define DISPLAY_LINE_FOUR_DELAY 20000 //ms
#define DISPLAY_LINE_FIVE_DELAY 10000 //ms
#define DISPLAY_LINE_SIX_DELAY 10000 //ms
#define DISPLAY_LINE_ONE 10000 //ms

void display_Task(void* pvParameters) {

    uint32_t lastLineOneUpdate = 0;
    uint32_t lastLineTwoUpdate = 0;
    uint32_t lastLineThreeUpdate = 0;
#ifdef GPS_ENABLED
    uint32_t lastGPSUpdate = 0;
#endif
    uint32_t lastLineFourUpdate = 0;
    uint32_t lastLineFiveUpdate = 0;
    uint32_t lastLineSixUpdate = 0;
    uint32_t lastLineSevenUpdate = 0;

    while (true) {
        //Update line one every DISPLAY_LINE_ONE ms
        if (millis() - lastLineOneUpdate > DISPLAY_LINE_ONE) {
            lastLineOneUpdate = millis();
            // float batteryVoltage = getBatteryVoltage();
            // Given the previous float value, convert it into string with 2 decimal places
            bool isConnected = WiFi.isConnected() || loraMeshService.hasGateway();
            String lineOne = "LoRaTRUST-  " + String(isConnected ? "CON" : "NC");
            //String lineOne = "LoRaMesh-  " + String(isConnected ? "CON" : "NC");

            Screen.changeLineOne(lineOne);
        }

        //Update line two every DISPLAY_LINE_TWO_DELAY ms
        if (millis() - lastLineTwoUpdate > DISPLAY_LINE_TWO_DELAY) {
            lastLineTwoUpdate = millis();
            String lineTwo = String(loraMeshService.getLocalAddress(), HEX);

            if (loraMeshService.isGateway())
                lineTwo += " | " + wiFiService.getIP();

            Screen.changeLineTwo(lineTwo);
        }

#ifdef GPS_ENABLED
        //Update line three every DISPLAY_LINE_THREE_DELAY ms
        // if (millis() - lastLineThreeUpdate > DISPLAY_LINE_THREE_DELAY) {
        //     lastLineThreeUpdate = millis();
        //     String lineThree = gpsService.getGPSUpdatedWait();
        //     if (lineThree.begin() != "G")
        //         Screen.changeLineThree(lineThree);
        // }

        //Update GPS every UPDATE_GPS_DELAY ms
        if (millis() - lastGPSUpdate > UPDATE_GPS_DELAY) {
            lastGPSUpdate = millis();
            String lineThree = gpsService.getGPSUpdatedWait();
            if (lineThree.startsWith("G") != 1)
                Screen.changeLineThree(lineThree);
        }
#endif

        Screen.drawDisplay();
        vTaskDelay(DISPLAY_TASK_DELAY / portTICK_PERIOD_MS);
    }
}

void createUpdateDisplay() {
    int res = xTaskCreate(
        display_Task,
        "Display Task",
        2048,
        (void*) 1,
        2,
        &display_TaskHandle);
    if (res != pdPASS) {
        Log.errorln(F("Display Task creation gave error: %d"), res);
        createUpdateDisplay();
    }
}

void initDisplay() {
    Screen.initDisplay();
    createUpdateDisplay();
}
#pragma endregion
#endif

#pragma region Wire

void initWire() {
    Wire.begin(I2C_SDA, I2C_SCL);
}

// TODO: The following line could be removed if we add the files in /src to /lib. However, at this moment, it generates a lot of errors
// TODO: https://docs.platformio.org/en/stable/advanced/unit-testing/structure/shared-code.html#unit-testing-shared-code

#ifndef PIO_UNIT_TESTING

void setup() {
    // Initialize Serial Monitor
    Serial.begin(115200);

    // Initialize Log
    Log.begin(LOG_LEVEL_VERBOSE, &Serial);

    // Initialize Wire
    initWire();

    Log.verboseln("Heap before initManager: %d", ESP.getFreeHeap());

    // Initialize Manager
    initManager();

    Log.verboseln("Heap after initManager: %d", ESP.getFreeHeap());

    // Initialize WiFi
    initWiFi();

    Log.verboseln("Heap after initWiFi: %d", ESP.getFreeHeap());

#ifdef MQTT_ENABLED
    // Initialize MQTT
    initMQTT();
    Log.verboseln("Heap after initMQTT: %d", ESP.getFreeHeap());
#endif

    

    //Initialize Wallet
    initWallet();

    Log.verboseln("Heap after initWallet: %d", ESP.getFreeHeap());

    // Initialize AXP192
    // initAXP();

#ifdef GPS_ENABLED
    // Initialize GPS
    initGPS();
    Log.verboseln("Heap after initGPS: %d", ESP.getFreeHeap());
#endif

    

    // Initialize LoRaMesh
    initLoRaMesher();

    Log.verboseln("Heap after initLoRaMesher: %d", ESP.getFreeHeap());

#ifdef BLUETOOTH_ENABLED
    // Initialize Bluetooth
    initBluetooth();
    Log.verboseln("Heap after initBluetooth: %d", ESP.getFreeHeap());
#endif

    

#ifdef LED_ENABLED
    // Initialize Led
    initLed();
    Log.verboseln("Heap after initLed: %d", ESP.getFreeHeap());

#endif

#ifdef CDP_ENABLED
    // Initialize Cdp
    initCdp();
    Log.verboseln("Heap after initCdp: %d", ESP.getFreeHeap());
#endif

    // Initialize Metadata
#ifdef METADATA_ENABLED
    initMetadata();
    Log.verboseln("Heap after initMetadata: %d", ESP.getFreeHeap());
#endif

    

#ifdef TEMPERATURE_ENABLED
    // Initialize Temperature
    initTemperature();
    Log.verboseln("Heap after initTemperature: %d", ESP.getFreeHeap());
#endif

    

#ifdef DISPLAY_ENABLED
    // Initialize Display
    initDisplay();
    Log.verboseln("Heap after initDisplay: %d", ESP.getFreeHeap());
#endif

    Log.verboseln(F("Setup finished"));

#ifdef LED_ENABLED
    // Blink 2 times to show that the device is ready
    Helper::ledBlink(2, 100);
#endif
}

void loop() {

    // Serial.printf("FREE HEAP: %d, %d, %d;\n", ESP.getFreeHeap(), ESP.getMaxAllocHeap(), ESP.getMinFreeHeap());

    // vTaskDelay(1000 / portTICK_PERIOD_MS);

    //Suspend this task
    vTaskSuspend(NULL);
}

#endif
#include "temperature.h"

void Temperature::initTemperature() {
    initializeTemperatureAndHumiditySensor();

    initializeSoilTemperatureAndMoistureSensor();

    initializeWaterLevelSensor();

    createTemperatureTask();

    Log.noticeln(F("Temperature initialized"));

    startTemperature();
}

void Temperature::initializeTemperatureAndHumiditySensor() {
    sht4x.begin(Wire);

    uint16_t error;
    char errorMessage[256];

    uint32_t serialNumber;
    error = sht4x.serialNumber(serialNumber);
    if (error) {
        Serial.print("Error trying to execute serialNumber(): ");
        errorToString(error, errorMessage, 256);
        Serial.println(errorMessage);
    }
    else {
        Serial.print("Serial Number: ");
        Serial.println(serialNumber);
    }
}

void Temperature::initializeSoilTemperatureAndMoistureSensor() {
    soilSensor.begin();
}

void Temperature::startTemperature() {
    running = true;
    xTaskNotifyGive(temperature_TaskHandle);

    Log.noticeln(F("Temperature task started"));
}

void Temperature::stopTemperature() {
    running = false;

    Log.noticeln(F("Temperature task stopped"));
}

float Temperature::readValue() {
    return readValueWait(0);
}

float Temperature::readValueWait(uint8_t retries) {
    // uint16_t error;
    // char errorMessage[256];
    // delay(2000);

    // int16_t distance;

    // float temperature;
    // float humidity;
    // error = sht4x.measureHighPrecision(temperature, humidity);
    // if (error) {
    //     Serial.print("Error trying to execute measureHighPrecision(): ");
    //     errorToString(error, errorMessage, 256);
    //     Serial.println(errorMessage);
    // }

    // float temperature = DEVICE_DISCONNECTED_C;

    // previousValues[previousValuesIndex] = temperature;
    // previousValuesIndex = (previousValuesIndex + 1) % STORED_SENSOR_DATA;

    // readValues++;

    return -127;
}

String Temperature::getJSON(DataMessage* message) {
    SensorMessageGeneric* sensorMessage = (SensorMessageGeneric*) message;

    switch (sensorMessage->sensorType) {
        case SensorTemperature:
            return getJSONTemperature((TemperatureMessage*) message);
        case SensorSoil:
            return getJSONSoil((SoilSensorMessage*) message);
        case SensorGround:
            return getJSONGround((GroundSensorMessage*) message);
        default:
            return "";
    }
}

void Temperature::createTemperatureTask() {
    int res = xTaskCreate(
        temperatureLoop,
        "Temperature Task",
        4096,
        (void*) 1,
        2,
        &temperature_TaskHandle);
    if (res != pdPASS) {
        Log.errorln(F("Temperature task handle error: %d"), res);
        createTemperatureTask();
    }
}

void Temperature::temperatureLoop(void*) {
    Temperature& temperature = Temperature::getInstance();
    Metadata& metadata = Metadata::getInstance();
    uint32_t lastMetadataSent = 0;

    while (true) {
        if (!temperature.running)
            ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        else {

            uint32_t sendEvery = temperature.readEveryMs / (3 * portTICK_PERIOD_MS);

            vTaskDelay(sendEvery);

            Log.verboseln(F("Free heap before send: %d"), ESP.getFreeHeap());

            temperature.readAndSendTemperatureAndHumidity();

            vTaskDelay(sendEvery);

            temperature.readAndSendSoilTemperatureMoistureAndDistance();

            vTaskDelay(sendEvery);

            // temperature.readAndSendGroundSensors();

            if (millis() - lastMetadataSent > METADATA_UPDATE_DELAY) {
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                metadata.createAndSendMetadata();
                lastMetadataSent = millis();
            }

            Log.verboseln(F("Free heap after send: %d"), ESP.getFreeHeap());
        }
    }
}

void Temperature::readAndSendTemperatureAndHumidity() {
    uint16_t error;
    char errorMessage[256];

    float temp;
    float humidity;
    error = sht4x.measureHighPrecision(temp, humidity);
    if (error) {
        Serial.print("Error trying to execute measureHighPrecision(): ");
        errorToString(error, errorMessage, 256);
        Serial.println(errorMessage);
    }
    else {
        Serial.print("Temperature:  ");
        Serial.print(temp);
        Serial.print("\t");
        Serial.print("Humidity:  ");
        Serial.print(humidity);
        Serial.println(" %");
    }

    sendTemperatureAndHumidity(temp, humidity);
}

void Temperature::readAndSendSoilTemperatureMoistureAndDistance() {
    soilSensor.wakeUp();

    float temperature;
    soilSensor.readTemperatureCelsius(&temperature);
    Serial.print("Soil Temperature:  ");
    Serial.print(temperature);
    Serial.println("°C");

    uint16_t moisture;
    soilSensor.readMoistureRaw(&moisture);

    Serial.print("Soil Moisture:  ");
    Serial.println(moisture);

    // TODO: Check if this is the same for all sensors, see the datasheet

    uint16_t moisturePercent = map(moisture, 6100, 13200, 0, 100);

    // float moisturePercent = static_cast<float>(moisture - 6100) / static_cast<float>(13120 - 6100);

    Serial.print("Soil Moisture 2:  ");
    Serial.print(moisturePercent);
    Serial.println("%");

    soilSensor.sleep();

    float distance = readAndSendWaterLevel();

    sendSoilTemperatureMoistureAndDistance(temperature, moisturePercent, distance);
}

void Temperature::sendSoilTemperatureMoistureAndDistance(float temperature, uint16_t moisture, float distance) {
    SoilSensorMessage* message = getSoilTemperatureMoistureAndDistanceMessage(temperature, moisture, distance);
    MessageManager::getInstance().sendMessage(messagePort::MqttPort, (DataMessage*) message);
    delete message;
}

SoilSensorMessage* Temperature::getSoilTemperatureMoistureAndDistanceMessage(float temperature, uint16_t moisture, float distance) {

    SoilSensorMessage* soilSensorMessage = new SoilSensorMessage();

    soilSensorMessage->setTemperature(temperature);
    soilSensorMessage->setMoisture(moisture);
    soilSensorMessage->setDistance(distance);

    soilSensorMessage->appPortDst = appPort::MQTTApp;
    soilSensorMessage->appPortSrc = appPort::TemperatureSensorApp;
    soilSensorMessage->addrSrc = LoraMesher::getInstance().getLocalAddress();
    soilSensorMessage->addrDst = 0;
    soilSensorMessage->messageId = sensorMessageId++;

    soilSensorMessage->gps = GPSService::getInstance().getGPSMessage();

    soilSensorMessage->messageSize = sizeof(SoilSensorMessage) - sizeof(DataMessageGeneric);

    //signData(soilSensorMessage);

    return soilSensorMessage;
}

void Temperature::signData(SoilSensorMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    getJSONDataObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    //Wallet::getInstance().signJson(json, message->signature);
}

void Temperature::getJSONDataObject(JsonObject& doc, SoilSensorMessage* message) {
    message->serialize(doc);
}

void Temperature::getJSONSignObject(JsonObject& doc, SoilSensorMessage* message) {
    //message->serializeSignature(doc);
}

String Temperature::getJSONSoil(SoilSensorMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    JsonObject dataObj = jsonObj.createNestedObject("data");

    getJSONDataObject(dataObj, message);

    //getJSONSignObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    return json;
}

void Temperature::readAndSendGroundSensors() {
    // PH.send_read_cmd();
    // RTD.send_read_cmd();

    // vTaskDelay(1000 / portTICK_PERIOD_MS);

    // float ph = receive_reading(PH);

    // float rtd = receive_reading(RTD);

    // sendGroundSensors(rtd, ph);
}

void Temperature::sendGroundSensors(float rtd, float ph) {
    GroundSensorMessage* message = getGroundSensorsMessage(rtd, ph);
    MessageManager::getInstance().sendMessage(messagePort::MqttPort, (DataMessage*) message);
    delete message;
}

GroundSensorMessage* Temperature::getGroundSensorsMessage(float rtd, float ph) {

    GroundSensorMessage* groundSensorMessage = new GroundSensorMessage();

    groundSensorMessage->setTemperature(rtd);
    groundSensorMessage->setPH(ph);

    groundSensorMessage->appPortDst = appPort::MQTTApp;
    groundSensorMessage->appPortSrc = appPort::TemperatureSensorApp;
    groundSensorMessage->addrSrc = LoraMesher::getInstance().getLocalAddress();
    groundSensorMessage->addrDst = 0;
    groundSensorMessage->messageId = sensorMessageId++;

    groundSensorMessage->gps = GPSService::getInstance().getGPSMessage();

    groundSensorMessage->messageSize = sizeof(GroundSensorMessage) - sizeof(DataMessageGeneric);

    //signData(groundSensorMessage);

    return groundSensorMessage;
}

void Temperature::signData(GroundSensorMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    getJSONDataObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    //Wallet::getInstance().signJson(json, message->signature);
}

void Temperature::getJSONDataObject(JsonObject& doc, GroundSensorMessage* message) {
    message->serialize(doc);
}

void Temperature::getJSONSignObject(JsonObject& doc, GroundSensorMessage* message) {
    //message->serializeSignature(doc);
}

String Temperature::getJSONGround(GroundSensorMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    JsonObject dataObj = jsonObj.createNestedObject("data");

    getJSONDataObject(dataObj, message);

    //getJSONSignObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    return json;
}

float Temperature::receive_reading(Ezo_board& Sensor) {
    // function to decode the reading after the read command was issued

    Serial.print(Sensor.get_name());  // print the name of the circuit getting the reading
    Serial.print(": ");

    Sensor.receive_read_cmd();  //get the response data and put it into the [Sensor].reading variable if successful

    switch (Sensor.get_error())  //switch case based on what the response code is.
    {
        case Ezo_board::SUCCESS:
            return Sensor.get_last_received_reading();
            // Serial.println();  //the command was successful, print the reading
            break;

        case Ezo_board::FAIL:
            Serial.print("Failed ");  //means the command has failed.
            break;

        case Ezo_board::NOT_READY:
            Serial.print("Pending ");  //the command has not yet been finished calculating.
            break;

        case Ezo_board::NO_DATA:
            Serial.print("No Data ");  //the sensor has no data to send.
            break;
    }

    return 0;
}

void Temperature::initializeWaterLevelSensor() {
    if (!vl53.begin(0x29, &Wire)) {
        Serial.print(F("Error on init of VL sensor: "));
        Serial.println(vl53.vl_status);
    }
    Serial.println(F("VL53L1X sensor OK!"));

    Serial.print(F("Sensor ID: 0x"));
    Serial.println(vl53.sensorID(), HEX);

    if (!vl53.startRanging()) {
        Serial.print(F("Couldn't start ranging: "));
        Serial.println(vl53.vl_status);
    }
    Serial.println(F("Ranging started"));

    // Valid timing budgets: 15, 20, 33, 50, 100, 200 and 500ms!
    vl53.setTimingBudget(50);
    Serial.print(F("Timing budget (ms): "));
    Serial.println(vl53.getTimingBudget());
}

float Temperature::readAndSendWaterLevel() {
    float distance = -1;
    if (vl53.dataReady()) {
        // new measurement for the taking!
        distance = vl53.distance();
        if (distance == -1) {
            // something went wrong!
            Serial.print(F("Couldn't get distance: "));
            Serial.println(vl53.vl_status);
            return -1;
        }

        // The sensor is 400mm above the water level so we need to invert the distance
        distance = (distance - 400) * -1;

        Serial.print(F("Distance: "));
        Serial.print(distance);
        Serial.println(" mm");

        // data is read out, time for another reading!
        vl53.clearInterrupt();
    }
    return distance;
}

void Temperature::sendTemperatureAndHumidity(float temperature, float humidity) {
    TemperatureMessage* message = getTemperatureAndHumidityMessage(temperature, humidity);
    MessageManager::getInstance().sendMessage(messagePort::MqttPort, (DataMessage*) message);
    delete message;
}

TemperatureMessage* Temperature::getTemperatureAndHumidityMessage(float temperature, float humidity) {

    TemperatureMessage* temperatureMessage = new TemperatureMessage();

    temperatureMessage->setTemperature(temperature);
    temperatureMessage->setHumidity(humidity);

    temperatureMessage->appPortDst = appPort::MQTTApp;
    temperatureMessage->appPortSrc = appPort::TemperatureSensorApp;
    temperatureMessage->addrSrc = LoraMesher::getInstance().getLocalAddress();
    temperatureMessage->addrDst = 0;
    temperatureMessage->messageId = sensorMessageId++;

    temperatureMessage->gps = GPSService::getInstance().getGPSMessage();

    temperatureMessage->messageSize = sizeof(TemperatureMessage) - sizeof(DataMessageGeneric);

    //signData(temperatureMessage);

    return temperatureMessage;
}

void Temperature::signData(TemperatureMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    getJSONDataObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    //Wallet::getInstance().signJson(json, message->signature);
    // memcpy(message->signature, signStr.c_str(), signStr.length());
}

void Temperature::getJSONDataObject(JsonObject& doc, TemperatureMessage* message) {
    message->serialize(doc);
}

void Temperature::getJSONSignObject(JsonObject& doc, TemperatureMessage* message) {
    //message->serializeSignature(doc);
}

String Temperature::getJSONTemperature(TemperatureMessage* message) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    JsonObject dataObj = jsonObj.createNestedObject("data");

    getJSONDataObject(dataObj, message);

    //getJSONSignObject(jsonObj, message);

    String json;
    serializeJson(doc, json);

    return json;
}

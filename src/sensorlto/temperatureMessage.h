#pragma once

#include <Arduino.h>

#include "sensorMessage.h"

#pragma pack(1)
class TemperatureMessage: public SensorMessageGeneric {
public:
    float temperature;
    float humidity;

    TemperatureMessage() {
        sensorType = SensorTemperature;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float value) {
        temperature = value;
    }

    float getHumidity() {
        return humidity;
    }

    void setHumidity(float value) {
        humidity = value;
    }

    void serialize(JsonObject& doc) {
        SensorMessageGeneric* message = (SensorMessageGeneric*) this;
        message->serialize(doc);

        JsonArray measurements = doc.createNestedArray("message");
        JsonObject tempObj = measurements.createNestedObject();
        tempObj["measurement"] = temperature;
        tempObj["type"] = "temperature";

        JsonObject humObj = measurements.createNestedObject();
        humObj["measurement"] = humidity;
        humObj["type"] = "humidity";
    }
};

#pragma pack()
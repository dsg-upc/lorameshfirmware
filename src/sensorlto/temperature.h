#pragma once

#include <Arduino.h>

#include <OneWire.h>

#include <SensirionI2CSht4x.h>

#include <SoilSensor.h>

#include <Ezo_i2c.h>  // Link: https://github.com/Atlas-Scientific/Ezo_I2c_lib)

#include "Adafruit_VL53L1X.h"

#include <ArduinoLog.h>

#include "sensor.h"

#include "temperatureCommandService.h"

#include "temperatureMessage.h"

#include "sensorlto/metadata.h"

#include "soilSensorMessage.h"

#include "groundSensorMessage.h"

class Temperature: public Sensor<float> {
public:
    /**
     * @brief Construct a new GPSService object
     *
     */
    static Temperature& getInstance() {
        static Temperature instance;
        return instance;
    }

    void initTemperature();

    void startTemperature();

    void stopTemperature();

    float readValue();

    float readValueWait(uint8_t retries);

    TemperatureCommandService* temperatureCommandService = new TemperatureCommandService();

    String getJSON(DataMessage* message);

private:
    Temperature(): Sensor(TemperatureSensorApp, "Temperature", TEMP_UPDATE_DELAY) {
        commandService = temperatureCommandService;
    };

    TaskHandle_t temperature_TaskHandle = NULL;

    void createTemperatureTask();

    static void temperatureLoop(void*);

    bool running = false;



#pragma region Air Sensor
    //Air Sensor
    SensirionI2CSht4x sht4x;

    void initializeTemperatureAndHumiditySensor();

    void readAndSendTemperatureAndHumidity();

    void sendTemperatureAndHumidity(float temperature, float humidity);

    TemperatureMessage* getTemperatureAndHumidityMessage(float temperature, float humidity);

    void signData(TemperatureMessage* message);

    void getJSONDataObject(JsonObject& doc, TemperatureMessage* message);

    void getJSONSignObject(JsonObject& doc, TemperatureMessage* message);

    String getJSONTemperature(TemperatureMessage* message);

#pragma endregion


#pragma region Soil Sensor
    //Soil Sensor
    OneWire oneWire = OneWire(SOIL_SENSOR_PIN);
    SoilSensor soilSensor = SoilSensor(&oneWire);

    void initializeSoilTemperatureAndMoistureSensor();

    void readAndSendSoilTemperatureMoistureAndDistance();

    void sendSoilTemperatureMoistureAndDistance(float temperature, uint16_t moisture, float distance);

    SoilSensorMessage* getSoilTemperatureMoistureAndDistanceMessage(float temperature, uint16_t moisture, float distance);

    void signData(SoilSensorMessage* message);

    void getJSONDataObject(JsonObject& doc, SoilSensorMessage* message);

    void getJSONSignObject(JsonObject& doc, SoilSensorMessage* message);

    String getJSONSoil(SoilSensorMessage* message);

#pragma endregion

#pragma region Ground Sensors

    // Ezo_board RTD = Ezo_board(102, "RTD");  //create a PH circuit object, who's address is 99 and name is "PH"
    // Ezo_board PH = Ezo_board(99, "PH");  //create a PH circuit object, who's address is 99 and name is "PH"

    // void initializeGroundSensors();

    void readAndSendGroundSensors();

    void sendGroundSensors(float rtd, float ph);

    GroundSensorMessage* getGroundSensorsMessage(float rtd, float ph);

    void signData(GroundSensorMessage* message);

    void getJSONDataObject(JsonObject& doc, GroundSensorMessage* message);

    void getJSONSignObject(JsonObject& doc, GroundSensorMessage* message);

    String getJSONGround(GroundSensorMessage* message);

    float receive_reading(Ezo_board& Sensor);

#pragma endregion

#pragma region Water Level
    // Distance/Water Level Sensor
    Adafruit_VL53L1X vl53 = Adafruit_VL53L1X();

    void initializeWaterLevelSensor();

    float readAndSendWaterLevel();

#pragma endregion
};
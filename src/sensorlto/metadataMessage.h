#pragma once

#include <Arduino.h>

#include <ArduinoLog.h>

#include "message/dataMessage.h"

#include "gps/gpsMessage.h"

#include "sensorlto/metadataSensorMessage.h"

#include "sensorlto/signatureMessage.h"

#pragma pack(1)
class MetadataMessage final: public DataMessageGeneric {//, public SignatureMessage {
public:
    MetadataMessage() {
    }

    GPSMessage gps;
    int metadataSendTimeInterval;
    uint8_t i, metadataSize = 20;
    // MetadataSensorMessage sensorMetadata[];

    void serialize(JsonObject& doc) {
        // Call the base class serialize function
        ((DataMessageGeneric*) (this))->serialize(doc);

        // Add the GPS data to the JSON object
        gps.serialize(doc);

        doc["message_type"] = "metadata";

        // Add the derived class data to the JSON object
        doc["metadata_send_time_interval"] = metadataSendTimeInterval;


        JsonArray sensorsArray = doc.createNestedArray("message");

        for(i=0;i<metadataSize;i++){
            sensorsArray.add(i);
        }
        // for (int i = 0; i < metadataSize; i++) {
        //     JsonObject sensorObject = sensorsArray.createNestedObject();
        //     sensorMetadata[i].serialize(sensorObject);
        //     // TODO: Add the sensor id to the metadata message
        //     sensorObject["id"] = i;
        // }
    }
};

#pragma pack()

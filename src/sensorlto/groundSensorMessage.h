#pragma once

#include <Arduino.h>

#include "sensorMessage.h"

#pragma pack(1)
class GroundSensorMessage: public SensorMessageGeneric {
public:
    float temperature;
    float ph;

    GroundSensorMessage() {
        sensorType = SensorGround;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float value) {
        temperature = value;
    }

    float getPH() {
        return ph;
    }

    void setPH(float value) {
        ph = value;
    }


    void serialize(JsonObject& doc) {
        SensorMessageGeneric* message = (SensorMessageGeneric*) this;
        message->serialize(doc);

        JsonArray measurements = doc.createNestedArray("message");
        JsonObject tempObj = measurements.createNestedObject();
        tempObj["measurement"] = temperature;
        tempObj["type"] = "Soil_Temperature";

        JsonObject phObj = measurements.createNestedObject();
        phObj["measurement"] = ph;
        phObj["type"] = "Soil_PH";
    }
};

#pragma pack()
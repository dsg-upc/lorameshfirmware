#pragma once

#include <Arduino.h>

#include "sensorMessage.h"

#pragma pack(1)
class SoilSensorMessage: public SensorMessageGeneric {
public:
    float temperature;
    float distance;
    uint16_t moisture;

    SoilSensorMessage() {
        sensorType = SensorSoil;
    }

    float getTemperature() {
        return temperature;
    }

    void setTemperature(float value) {
        temperature = value;
    }

    float getMoisture() {
        return moisture;
    }

    void setMoisture(uint16_t value) {
        moisture = value;
    }

    float getDistance() {
        return distance;
    }

    void setDistance(float value) {
        distance = value;
    }


    void serialize(JsonObject& doc) {
        SensorMessageGeneric* message = (SensorMessageGeneric*) this;
        message->serialize(doc);

        JsonArray measurements = doc.createNestedArray("message");
        JsonObject tempObj = measurements.createNestedObject();
        tempObj["measurement"] = temperature;
        tempObj["type"] = "Soil_Temperature_Low_Res";

        JsonObject humObj = measurements.createNestedObject();
        humObj["measurement"] = moisture;
        humObj["type"] = "Soil_Moisture";

        JsonObject distObj = measurements.createNestedObject();
        distObj["measurement"] = distance;
        distObj["type"] = "Water_Level";
    }
};

#pragma pack()
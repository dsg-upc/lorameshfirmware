#pragma once

#include <Arduino.h>

#include <ArduinoLog.h>

#include "config.h"

#include "message/messageService.h"

#include "message/messageManager.h"

#include "sensorlto/metadataMessage.h"

#include "sensorlto/metadataCommandService.h"

#include "wallet/wallet.h"

class Metadata: public MessageService {
public:
    static Metadata& getInstance() {
        static Metadata instance;
        return instance;
    }

    void initMetadata();

    void startMetadata();

    void stopMetadata();

    void createAndSendMetadata();

    String getJSON(DataMessage* message);

    MetadataCommandService* metadataCommandService = new MetadataCommandService();

private:
    Metadata(): MessageService(MetadataApp, "Metadata") {
        commandService = metadataCommandService;
    };

    TaskHandle_t metadata_TaskHandle = NULL;

    void createMetadataTask();

    static void metadataLoop(void*);

    bool running = true;

    void signData(MetadataMessage* metadataMessage);

    uint8_t metadataId = 0;

    void getJSONDataObject(JsonObject& doc, MetadataMessage* metadataMessage);

    void getJSONSignObject(JsonObject& doc, MetadataMessage* metadataMessage);
};
#include "metadata.h"
#include "sensorlto/temperature.h"

void Metadata::initMetadata() {
    Log.notice(F("Initializing metadata" CR));
    createMetadataTask();  //FF acrtivated
}

void Metadata::startMetadata() {
    Log.notice(F("Starting metadata" CR));
    running = true;
    xTaskNotifyGive(metadata_TaskHandle);
}

void Metadata::stopMetadata() {
    Log.notice(F("Stopping metadata" CR));
    running = false;
}

String Metadata::getJSON(DataMessage* message) {
    MetadataMessage* metadataMessage = (MetadataMessage*) message;
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    JsonObject dataObj = jsonObj.createNestedObject("data");

    getJSONDataObject(dataObj, metadataMessage);

    // getJSONSignObject(jsonObj, metadataMessage);

    String json;
    serializeJson(doc, json);

    return json;
}


void Metadata::createMetadataTask() {
    int res = xTaskCreate(
        metadataLoop,
        "Metadata Task",
        4096,
        (void*) 1,
        2,
        &metadata_TaskHandle
    );

    if (res != pdPASS) {
        Log.error(F("Failed to create metadata task" CR));
    }
}

void Metadata::metadataLoop(void* pvParameters) {
    Log.notice(F("Metadata task started" CR));

    Metadata& metadata = Metadata::getInstance();

    while (true) {
        if (!metadata.running) {
            ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
            Log.notice(F("Metadata task started with !metadata.running" CR));
        }
        else {
            Log.notice(F("Metadata task started with metadata.running" CR));
            metadata.createAndSendMetadata(); 
            vTaskDelay(METADATA_UPDATE_DELAY / portTICK_PERIOD_MS);

        }
    }
}

void Metadata::createAndSendMetadata() {
    // uint8_t metadataSize = 1; //TODO: This should be dynamic with an array of sensors
    // uint16_t metadataSensorSize = metadataSize * sizeof(MetadataSensorMessage);
    uint16_t messageWithHeaderSize = sizeof(MetadataMessage);// + metadataSensorSize;

    MetadataMessage* message = (MetadataMessage*) malloc(messageWithHeaderSize);

    Log.verboseln(F("Sending metadata message"));

    if (message) {

        message->appPortDst = appPort::MQTTApp;
        message->appPortSrc = appPort::MetadataApp;
        message->addrSrc = LoraMesher::getInstance().getLocalAddress();
        message->addrDst = 0;
        message->messageId = metadataId++;

        message->messageSize = messageWithHeaderSize - sizeof(DataMessageGeneric);
        message->gps = GPSService::getInstance().getGPSMessage();
        message->metadataSendTimeInterval = METADATA_UPDATE_DELAY;

        // message->metadataSize = metadataSize;

        //TODO: This should be a vector of sensors
        // Temperature& temperature = Temperature::getInstance();

        // MetadataSensorMessage* tempMetadata = temperature.getMetadataMessage();
        // memcpy(message->sensorMetadata, tempMetadata, sizeof(MetadataSensorMessage));

        // signData(message);

        MessageManager::getInstance().sendMessage(messagePort::MqttPort, (DataMessage*) message);

        free(message);
    }
    else {
        Log.errorln(F("Failed to allocate memory for metadata message"));
    }
}

void Metadata::signData(MetadataMessage* metadataMessage) {
    DynamicJsonDocument doc(1024);
    JsonObject jsonObj = doc.to<JsonObject>();
    getJSONDataObject(jsonObj, metadataMessage);

    // Print the object to the serial port
    // serializeJsonPretty(doc, Serial);

    String json;
    serializeJson(doc, json);

    // Wallet::getInstance().signJson(json, metadataMessage->signature);
    // memcpy(metadataMessage->signature, signStr.c_str(), signStr.length());
}

void Metadata::getJSONDataObject(JsonObject& doc, MetadataMessage* metadataMessage) {
    metadataMessage->serialize(doc);
}

void Metadata::getJSONSignObject(JsonObject& doc, MetadataMessage* metadataMessage) {
    // metadataMessage->serializeSignature(doc);
}

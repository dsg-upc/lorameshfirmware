#include <Arduino.h>
#include <unity.h>

#include "wallet/Wallet.h"

// void tearDown(void) {
// // clean stuff up here
// }

void setUp(void) {
    // set stuff up here
    Wallet::getInstance().begin();
    // Wallet::getInstance().setPrivateKey("9e2de20c744f19c2e5a95b894cd4c5e7c02288f5eac78780a4a73a1d04a449f5");
}

void test_wallet_private_key(void) {
    String privKey = Wallet::getInstance().getPrivateKey();
    String realPrivKey = "9e2de20c744f19c2e5a95b894cd4c5e7c02288f5eac78780a4a73a1d04a449f5";

    TEST_ASSERT_EQUAL_STRING(realPrivKey.c_str(), privKey.c_str());
}

void test_signHex(void) {
    Wallet& wallet = Wallet::getInstance();

    String data = "A0D0689E2926410B8E456bC16c8fe5E45b0De5E4";

    uint8_t result[65];

    String signature = wallet.signHex(data, result);

    String realSignature = "95e2156314e74933e33fb9f2cf41101c41b802c9d7e6025b3ee1b06337aa990d72a3f14dab85921aaad7e8e5a0f061975d2ab31725630c2aad1439cd8594c1bb1c";

    uint8_t realResult[65] = {
        149, 226, 21,  99,  20, 231,  73,  51, 227,  63, 185,
        242, 207, 65,  16,  28,  65, 184,   2, 201, 215, 230,
          2,  91, 62, 225, 176,  99,  55, 170, 153,  13, 114,
        163, 241, 77, 171, 133, 146,  26, 170, 215, 232, 229,
        160, 240, 97, 151,  93,  42, 179,  23,  37,  99,  12,
         42, 173, 20,  57, 205, 133, 148, 193, 187,  28
    };

    TEST_ASSERT_EQUAL_STRING(realSignature.c_str(), signature.c_str());

    TEST_ASSERT_EQUAL_UINT8_ARRAY(realResult, result, 65);

    data = "0xA0D0689E2926410B8E456bC16c8fe5E45b0De5E4";

    String signatureTwo = wallet.signHex(data, result);

    TEST_ASSERT_EQUAL_STRING(signature.c_str(), signatureTwo.c_str());
}

void test_signJson(void) {
    Wallet& wallet = Wallet::getInstance();

    String data = "{\"data\":{\"appDst\":8,\"appSrc\":4,\"msgId\":0,\"src\":56988,\"dst\":43572,\"size\":33,\"type\":2,\"lat\":0,\"long\":0,\"alt\":0,\"sat\":0,\"h\":9,\"m\":37,\"s\":46,\"d\":0,\"mo\":0,\"y\":2000}}";

    uint8_t result[65];

    String signature = wallet.signJson(data, result);

    String realSignature = "a7441f122ee211d3420b3fb59e997fc09064b68cfd69b0085be60fbfa45d42b47bb182798a8c6c60d880cbee06ed7375d6b1adf3e23d295aebe046295c8305011c";

    uint8_t realResult[65] = {
        167,  68,  31,  18,  46, 226,  17, 211,  66,  11,  63,
        181, 158, 153, 127, 192, 144, 100, 182, 140, 253, 105,
        176,   8,  91, 230,  15, 191, 164,  93,  66, 180, 123,
        177, 130, 121, 138, 140, 108,  96, 216, 128, 203, 238,
            6, 237, 115, 117, 214, 177, 173, 243, 226,  61,  41,
        90, 235, 224,  70,  41,  92, 131,   5,   1,  28
    };

    TEST_ASSERT_EQUAL_STRING(realSignature.c_str(), signature.c_str());

    TEST_ASSERT_EQUAL_UINT8_ARRAY(realResult, result, 65);
}

void testJSONRealData(void) {
    Wallet& wallet = Wallet::getInstance();

    String data = "{\"data\":{\"appPortDst\":3,\"appPortSrc\":10,\"messageId\":0,\"addrSrc\":56988,\"addrDst\":0,\"messageSize\":101,\"gps\":{\"latitude\":0,\"longitude\":0,\"altitude\":0,\"satellite_number\":0},\"timestamp\":\"2000-0-0T0:0:0Z\",\"messageType\":\"measurement\",\"message\":[{\"measurement\":\"-127.00\",\"type\":\"temperature\",\"id\":0}]}}";

    uint8_t result[65];

    String signature = wallet.signJson(data, result);

    String realSignature = "ca71281e67290bcc3c3944af6da6c22aa058ddaffad4eb045c3408f318e7eea84db793bcfe60633e0e4449c8db2750a92f53b26dd02f5733ff15e40aa3e0f5121b";

    uint8_t realResult[65] = {
        202, 113,  40,  30, 103,  41,  11, 204,  60,  57,  68,
        175, 109, 166, 194,  42, 160,  88, 221, 175, 250, 212,
        235,   4,  92,  52,   8, 243,  24, 231, 238, 168,  77,
        183, 147, 188, 254,  96,  99,  62,  14,  68,  73, 200,
        219,  39,  80, 169,  47,  83, 178, 109, 208,  47,  87,
        51, 255,  21, 228,  10, 163, 224, 245,  18,  27
    };

    TEST_ASSERT_EQUAL_STRING(realSignature.c_str(), signature.c_str());

    TEST_ASSERT_EQUAL_UINT8_ARRAY(realResult, result, 65);
}

void setup() {
    UNITY_BEGIN();    // IMPORTANT LINE!

    RUN_TEST(test_wallet_private_key);

    RUN_TEST(test_signHex);

    RUN_TEST(test_signJson);

    RUN_TEST(testJSONRealData);

    UNITY_END(); // stop unit testing
}

void loop() {
    // UNITY_END(); // stop unit testing
}